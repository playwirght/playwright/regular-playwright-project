To configure Jenkins run on localhost
-- Open terminal and run the command(port can be different)
java -jar jenkins.war --httpPort=8080 --enable-future-java

-- Open localhost with the port defined in the command above
localhost:8080

-- Unlock Jenking page
enter password logged in the terminal from previous step

--Install suggested plugins
