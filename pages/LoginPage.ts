import { expect, Locator, Page } from "@playwright/test";
import { BasicPage } from "./BasicPage";

export class LoginPage extends BasicPage {
    readonly userNameField: Locator;
    readonly passwordField: Locator;
    readonly signInLoginPageBtn: Locator;
    readonly errorMessage: Locator;
    readonly loginForm: Locator;

    constructor(page: Page) {
        super(page);
        this.userNameField = page.locator('#user_login');
        this.passwordField = page.locator('#user_password');
        this.signInLoginPageBtn = page.getByRole('button', { name: 'Sign in' });
        this.loginForm = page.locator('#login_form');
        this.errorMessage = page.locator('.alert-error');
    }

    async login(username: string, password: string) {
        await this.userNameField.fill(username);
        await this.passwordField.fill(password);
        await this.signInLoginPageBtn.click();
    }

    async assertInvalidLoginMessage() {
        await expect(this.page.getByText('Login and/or password are wrong')).toBeVisible()
    }

    async snapshotLoginForm() {
        await expect(await this.loginForm.screenshot()).toMatchSnapshot('login-form.png');
    }
    
    async snapshotErrorMessage() {
        await expect(await this.errorMessage.screenshot()).toMatchSnapshot('login-error.png');
    }
}