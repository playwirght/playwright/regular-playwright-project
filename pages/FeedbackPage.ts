import { expect, Locator, Page } from "@playwright/test";
import { BasicPage } from "./BasicPage";

export class FeedbackPage extends BasicPage {
    readonly name: Locator;
    readonly email: Locator;
    readonly subject: Locator;
    readonly comment: Locator;
    readonly clearButton: Locator;
    readonly sendMessageButton: Locator;

    constructor(page: Page) {
        super(page);
        this.name = page.locator('#name');
        this.email = page.locator('#email');
        this.subject = page.locator('#subject');
        this.comment = page.locator('#comment');
        this.clearButton = page.locator('input[name=clear]');
        this.sendMessageButton = page.locator('input[name=submit]');
    }

    async gotoFeedbackPage() {
        await this.page.goto('http://zero.webappsecurity.com/feedback.html');
    }

    async fillFeedback(name: string, email: string, subject: string, comment: string) {
        await this.name.fill(name);
        await this.email.fill(email);
        await this.subject.fill(subject);
        await this.comment.fill(comment);
    }

    async clickClearButton() {
        await this.clearButton.click();
    }

    async clickSendMessageButton() {
        await this.sendMessageButton.click();
    }

    async validateFeedbackSent() {
        await this.page.waitForSelector('#feedback-title');
    }

    async assertFeedbackFormBeingReset() {
        await expect(this.name).toBeEmpty();
        await expect(this.name).toBeEmpty();
    }
}