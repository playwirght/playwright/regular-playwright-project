import { expect, Locator, Page } from "@playwright/test";
import { BasicPage } from "./BasicPage";

export class PayBillsPage extends BasicPage {
    readonly payeeDropDown: Locator;
    readonly payeeDetailsAd: Locator;
    readonly payeeDetailsMessage: Locator;
    readonly accountDropDown: Locator;
    readonly amountField: Locator;
    readonly dateField: Locator;
    readonly payButton: Locator;
    readonly paymentNotification: Locator;

    constructor(page: Page) {
        super(page);
        this.payeeDropDown = page.locator('#sp_payee');
        this.payeeDetailsAd = page.locator('#sp_get_payee_details');
        this.payeeDetailsMessage = page.locator('#sp_payee_details');
        this.accountDropDown = page.locator('#sp_account');
        this.amountField = page.locator('#sp_amount');
        this.dateField = page.locator('#sp_date');
        this.payButton = page.locator('#pay_saved_payees');
        this.paymentNotification = page.locator('#alert_content > span');
    }

    async gotoPayeeBillsTab() {
        await this.page.goto('http://zero.webappsecurity.com/bank/pay-bills.html');
    }

    async selectPayeeDropdownValueByName(payeeName: string) {
        await this.payeeDropDown.selectOption(payeeName);
    }

    async clickPayeeDetails() {
        await this.payeeDetailsAd.click();
    }

    async selectAccountDropdownValueByName(accountName: string) {
        await this.accountDropDown.selectOption(accountName);
    }

    async inputAmountAndDate(amountVal: string, date: string) {
        await this.amountField.fill(amountVal);
        await this.dateField.fill(date);
    }

    async clickPayButton() {
        await this.payButton.click();
    }

    async validatePayBillsTitle() {
        await expect(this.page).toHaveTitle("Zero - Pay Bills");
    }

    async validatePayeeDetailsText(payeeDetailsText: string) {
        await expect(this.payeeDetailsMessage).toHaveText(payeeDetailsText);
    }

    async validateSuccesfullPaymentNotificationMessage(paymentNotification: string) {
        await expect(this.paymentNotification).toHaveText(paymentNotification);
    }
}