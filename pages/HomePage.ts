import { Locator, Page } from "@playwright/test";
import { BasicPage } from "./BasicPage";

export class HomePage extends BasicPage {
    readonly signInBtn: Locator;
    readonly searchBox: Locator;

    constructor(page: Page) {
        super(page);
        this.signInBtn = page.locator('#signin_button');
        this.searchBox = page.locator('#searchTerm');
    }

    async visit() {
        await this.page.goto('http://zero.webappsecurity.com/');
    }

    async clickSingInButton() {
        this.signInBtn.click();
    }

    async search(phrase: string) {
        await this.searchBox.fill(phrase);
        await this.page.keyboard.press('Enter');
    }
}