import { expect, Locator, Page } from "@playwright/test";
import { BasicPage } from "./BasicPage";

export class CurrencyExchangePage extends BasicPage {
    readonly currencyDropDown: Locator;
    readonly currencyDropDownOptions: Locator;
    readonly currencyMessage: Locator;
    readonly ammountField: Locator;
    readonly dollarRadioButton: Locator;
    readonly calculateCostsButton: Locator;
    readonly conversionAmountMessage: Locator;
    readonly purchaseButton: Locator;

    constructor(page: Page) {
        super(page);
        this.currencyDropDown = page.locator('#pc_currency');
        this.currencyDropDownOptions = page.locator('#pc_currency > option');
        this.currencyMessage = page.locator('//select[@id="pc_currency"]/following-sibling::p');
        this.ammountField = page.locator('#pc_amount');
        this.dollarRadioButton = page.locator('#pc_inDollars_false');
        this.calculateCostsButton = page.locator('#pc_calculate_costs');
        this.conversionAmountMessage = page.locator('#pc_conversion_amount');
        this.purchaseButton = page.locator('#purchase_cash');
    }

    async openPurchaseForeignCurrencyTab() {
        await this.page.getByRole('link', { name: 'Purchase Foreign Currency' }).click();
    }

    async clickCalculateCostsButton() {
        await this.calculateCostsButton.click();
    }

    async clickPurchaseButton() {
        await this.purchaseButton.click();
    }

    async selectDropDownOptionByName(optionName: string) {
        await this.currencyDropDown.click();
        await this.currencyDropDown.selectOption(optionName);
    }

    async setAmount(amount: string) {
        await this.ammountField.fill(amount);
    }

    async checkDollarRadioButton() {
        await this.dollarRadioButton.check();
    }

    async validatePurchaseFromOpened() {
        await this.page.waitForSelector('#pc_purchase_currency_form');
    }

    async validateEmpptyDropdownValue() {
        await expect(this.currencyDropDownOptions.first()).toHaveText('Select One');
    }

    async validateCurrencyMessage() {
        await expect(this.currencyMessage).toContainText("Today's Sell Rate: 1 euro (EUR) =");
    }

    async validateDollarRadioButtonSelected() {
        await expect(this.dollarRadioButton.isChecked()).toBeTruthy();
    }

    async validateConversionAmountMessage() {
        await expect(this.conversionAmountMessage).toContainText("10.00 euro (EUR) = ");
    }
}

// test('Positive test', async ({ page }) => {
//     await page.goto('http://zero.webappsecurity.com/bank/pay-bills.html');
//     await currencyExchangePage.openPurchaseForeignCurrencyTab();
//     await currencyExchangePage.validatePurchaseFromOpened();

//     await currencyExchangePage.validateEmpptyDropdownValue();selectDropDownOptionByName

//     await currencyExchangePage.selectDropDownOptionByName('EUR');
//     await currencyExchangePage.validateCurrencyMessage();

//     await currencyExchangePage.setAmount('10');
//     await currencyExchangePage.checkDollarRadioButton();
//     await currencyExchangePage.validateDollarRadioButtonSelected();

//     await currencyExchangePage.clickCalculateCostsButton();
//     await currencyExchangePage.validateConversionAmountMessage()

//     await currencyExchangePage.clickPurchaseButton();

//     await expect(page.locator('.alert-success #alert_content')).toHaveText("Foreign currency cash was successfully purchased.");
// })