import { expect, Locator, Page } from "@playwright/test";
import { BasicPage } from "./BasicPage";

export class AccountActivityPage extends BasicPage {
    readonly acountDropDown: Locator;
    readonly transactionsAmount: Locator;

    constructor(page: Page) {
        super(page)
        this.acountDropDown = page.locator('#aa_accountId');
        this.transactionsAmount = page.locator('#all_transactions_for_account tbody > tr');
    }

    async gotoAccountActivityTab() {
        await this.page.goto('http://zero.webappsecurity.com/bank/account-activity.html');
    }

    async selectAccountDropdownValueByName(accountName: string) {
        await this.acountDropDown.selectOption(accountName);
    }

    async validateTransactionsAmount(amount: number) {
        await expect(this.transactionsAmount).toHaveCount(amount);
    }

    async validateAccountActivityTitle() {
        await expect(this.page).toHaveTitle("Zero - Account Activity");
    }
}