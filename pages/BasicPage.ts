import { Page } from "@playwright/test";

export class BasicPage {
    readonly page: Page;

    constructor(page: Page) {
        this.page = page;
    }

    async wait() {
        //Added for ddemo purposes
        await this.page.setDefaultTimeout(500);
        await this.page.waitForLoadState();
    }
}