import { test } from '@playwright/test';
import { LoginPage } from '../../pages/LoginPage';
import { HomePage } from '../../pages/HomePage';

test.describe.only("Login Page Visual Test", () => {
    let loginPage: LoginPage;
    let homePage: HomePage;

    test.beforeEach(async ({ page }) => {
        loginPage = new LoginPage(page);
        homePage = new HomePage(page);

        await homePage.visit();
        await homePage.clickSingInButton();
    })

    test('Login From', async ({ page }) => {
        await loginPage.snapshotLoginForm();
    })

    test('Login Error Message', async ({ page }) => {
        await loginPage.login('invalid username', 'invalid password');
        await loginPage.snapshotErrorMessage();
    })
})