import { test, expect } from '@playwright/test'

test.describe("Visual Regresion Testing Example", () => {
    test('Full Page Snapshot', async ({ page }) => {
        await page.goto('https://www.example.com');
        expect(await page.screenshot()).toMatchSnapshot('homepage.png');
    })

    test('Single Snapshot', async ({ page }) => {
        await page.goto('https://www.example.com');
        const element = await page.$('h1');
        expect(await element.screenshot()).toMatchSnapshot('poge-title.png');
    })
})