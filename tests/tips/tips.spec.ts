import { test } from '@playwright/test'

test.describe.only('Tips and tricks', () => {
    test('Test Info Object', async ({ page }, testInfo) => {
        await page.goto('https://www.example.com')
        //console.log(testInfo)
    })

    test('Test Skip Browser', async ({ page, browserName }) => {
        test.skip(browserName === 'chromium', 'Feature not ready in chrome browser')
        await page.goto('https://www.example.com')
    })

    test('Test Fix me annotation', async ({ page, browserName }) => {
        test.fixme(browserName === 'chromium', 'Need to be fixed')
        await page.goto('https://www.example.com')
    })

    const people = ['Name_1', 'Name_2', 'Name_3', 'Name_4', 'Name_5'];

    for (const name of people) {
        test(`Paramatrized test ${name}`, async ({ page }) => {
            await page.goto('http://zero.webappsecurity.com/index.html');
            await page.locator('#searchTerm').fill(name);
            await page.waitForTimeout(3000);
        })
    }

    test('Mouse movement simulation', async ({ page }) => {
        await page.goto('https://www.example.com');
        await page.mouse.move(0, 0);
        await page.mouse.down();
        await page.mouse.up();
    })

    test('Multiple browser Tabs inside one borwser', async ({ browser }) => {
        const context = await browser.newContext();
        const page1 = await context.newPage();
        const page2 = await context.newPage();
        const page3 = await context.newPage();
        await page1.goto('https://www.example.com');
        await page2.goto('https://www.example.com');
        await page3.goto('https://www.example.com');
    })
})