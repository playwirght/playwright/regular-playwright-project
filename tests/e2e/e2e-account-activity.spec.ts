import { test } from '@playwright/test';
import { LoginPage } from '../../pages/LoginPage';
import { HomePage } from '../../pages/HomePage';
import { AccountActivityPage } from '../../pages/AccountActivityPage';

test.describe('Account activity', () => {
    let loginPage: LoginPage;
    let homePage: HomePage;
    let accountActivityPage: AccountActivityPage;

    test.beforeEach(async ({ page }) => {
        loginPage = new LoginPage(page);
        homePage = new HomePage(page);
        accountActivityPage = new AccountActivityPage(page);

        await homePage.visit();
        await homePage.clickSingInButton()
        await loginPage.login('username', 'password');
    })

    test('Positive test', async ({ page }) => {
        await accountActivityPage.gotoAccountActivityTab();
        await accountActivityPage.validateAccountActivityTitle();
        await accountActivityPage.validateTransactionsAmount(3);
        await accountActivityPage.selectAccountDropdownValueByName('Loan');
        await accountActivityPage.validateTransactionsAmount(2);
    })
})