import { test } from '@playwright/test';
import { LoginPage } from '../../pages/LoginPage';
import { HomePage } from '../../pages/HomePage';
import { PayBillsPage } from '../../pages/PaybillsPage';

test.describe('Account activity', () => {
    let loginPage: LoginPage;
    let homePage: HomePage;
    let payBillsPage: PayBillsPage;

    test.beforeEach(async ({ page }) => {
        loginPage = new LoginPage(page);
        homePage = new HomePage(page);
        payBillsPage = new PayBillsPage(page);

        await homePage.visit();
        await homePage.clickSingInButton()
        await loginPage.login('username', 'password');
    })

    test('Positive test', async ({ page }) => {
        await payBillsPage.gotoPayeeBillsTab();
        await payBillsPage.validatePayBillsTitle();
        await payBillsPage.selectPayeeDropdownValueByName('Apple');
        await payBillsPage.clickPayeeDetails();
        await payBillsPage.validatePayeeDetailsText('For 48944145651315 Apple account');
        await payBillsPage.selectAccountDropdownValueByName('Brokerage');
        await payBillsPage.inputAmountAndDate('25', '2023-10-05');
        await payBillsPage.clickPayButton();
        await payBillsPage.validateSuccesfullPaymentNotificationMessage('The payment was successfully submitted.');
    })
})