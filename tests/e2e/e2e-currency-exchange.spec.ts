import { test, expect } from '@playwright/test';
import { LoginPage } from '../../pages/LoginPage';
import { HomePage } from '../../pages/HomePage';
import { CurrencyExchangePage } from '../../pages/CurrencyExchangePage';
import { PayBillsPage } from '../../pages/PaybillsPage';


test.describe('Purchase foreign currency cash', () => {
    let loginPage: LoginPage;
    let homePage: HomePage;
    let payBillsPage: PayBillsPage;
    let currencyExchangePage: CurrencyExchangePage;

    test.beforeEach(async ({ page }) => {
        loginPage = new LoginPage(page);
        homePage = new HomePage(page);
        payBillsPage = new PayBillsPage(page);
        currencyExchangePage = new CurrencyExchangePage(page);

        await homePage.visit();        
        await homePage.clickSingInButton()
        await loginPage.login('username', 'password');
    })

    test('Positive test', async ({ page }) => {
        payBillsPage.gotoPayeeBillsTab();
        
        await currencyExchangePage.openPurchaseForeignCurrencyTab();
        await currencyExchangePage.validatePurchaseFromOpened();
        await currencyExchangePage.validateEmpptyDropdownValue();

        await currencyExchangePage.selectDropDownOptionByName('EUR');
        await currencyExchangePage.validateCurrencyMessage();

        await currencyExchangePage.setAmount('10');
        await currencyExchangePage.checkDollarRadioButton();
        await currencyExchangePage.validateDollarRadioButtonSelected();

        await currencyExchangePage.clickCalculateCostsButton();
        await currencyExchangePage.validateConversionAmountMessage()

        await currencyExchangePage.clickPurchaseButton();
    })
})