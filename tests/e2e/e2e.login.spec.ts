import { test, expect } from '@playwright/test';
import { LoginPage } from '../../pages/LoginPage';
import { HomePage } from '../../pages/HomePage';

test.describe("Login/Logout Flow", () => {
    let loginPage: LoginPage;
    let homePage: HomePage;

    test.beforeEach(async ({ page }) => {
        loginPage = new LoginPage(page);
        homePage = new HomePage(page);

        await homePage.visit();
        await homePage.clickSingInButton();
    })

    test('Negative scenario for login', async ({ page }) => {
        await loginPage.login('invalid username', 'invalid password');
        await loginPage.assertInvalidLoginMessage();
    })

    test.skip('Positive scenario for login + logout', async ({ page }) => {
        await loginPage.login('username', 'password');

        await expect(page.locator('.icon-user')).toBeVisible();

        await page.goto('http://zero.webappsecurity.com/logout/html');
        await expect(page).toHaveURL('http://zero.webappsecurity.com/logout/html');
    })
})