import { test } from '@playwright/test';
import { HomePage } from '../../pages/HomePage';
import { FeedbackPage } from '../../pages/FeedbackPage';

test.describe("Feedback form", () => {
    let homePage: HomePage;
    let feedbackPage: FeedbackPage;

    test.beforeEach(async ({ page }) => {
        homePage = new HomePage(page);
        feedbackPage = new FeedbackPage(page);

        await homePage.visit();
        await feedbackPage.gotoFeedbackPage();
    })

    test('Reset feedback form', async ({ page }) => {
        await feedbackPage.fillFeedback('name', 'email', 'subject', 'comment');
        await feedbackPage.clickClearButton();

        await feedbackPage.assertFeedbackFormBeingReset();
    })


    test('Submit feedback form', async ({ page }) => {
        await feedbackPage.fillFeedback('name', 'email', 'subject', 'comment');
        await feedbackPage.clickSendMessageButton();

        await feedbackPage.validateFeedbackSent();
    })
})