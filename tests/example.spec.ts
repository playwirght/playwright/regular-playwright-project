import { test, expect } from '@playwright/test'

test('Simple basic test', async ({ page }) => {
    await page.goto('https://www.example.com');
    await expect(await page.locator('h1')).toContainText('Example Domain');
})

test('Clicking on elements', async ({ page }) => {
    await page.goto('http://zero.webappsecurity.com/');
    await page.click('#signin_button');
    await page.click('text=Sign in');;
    await expect(page.locator('.alert-error')).toContainText('Login and/or password are wrong.');
})

test.skip('Selectors', async ({ page }) => {
    //Text
    await page.click('text= some text');

    //Css
    await page.locator('#id');
    await page.locator('.class')

    //Only visible css + visible
    await page.locator('.submit-button:visible')

    //Xpath
    await page.locator('//button')
})

test('Assertions', async ({ page }) => {
    await page.goto('https://www.example.com');
    await expect(page).toHaveURL('https://www.example.com');
    await expect(page).toHaveTitle('Example Domain');
    await page.pause();

    await expect(await page.locator('h1')).toContainText('Example Domain');
    await expect(await page.locator('h1')).toBeVisible();
    await expect(await page.locator('h1')).toHaveCount(1);
    await expect(await page.locator('h5')).not.toBeVisible();

})

test.describe.parallel.only('Hooks', () => {
    test.beforeEach(async({page}) => {
        await page.goto('https://www.example.com');
    })
    test('Screenshots full screen', async ({ page }) => {
        await page.screenshot({ path: 'screenshot.png', fullPage: true });  
    })

    test('Screenshots single emenent', async ({ page }) => {
        const element = await page.$('h1');
        await element.screenshot({ path: 'screenshot1.png'});
    })

})

