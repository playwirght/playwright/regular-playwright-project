import { expect, test } from '@playwright/test';

test.describe.parallel('API testing', () => {
    const baseURL = 'https://reqres.in/api';

    test('Simple api test - Assert response status - POSITIVE', async ({ request }) => {
        const response = await request.get(`${baseURL}/users/2`);
        expect(response.status()).toBe(200);

    })

    test('Simple api test - Assert response status - NEGATIVE', async ({ request }) => {
        const response = await request.get(`${baseURL}/users/2ws`);
        expect(response.status()).toBe(404);
    })

    test('GET request - Get User Detail', async ({ request }) => {
        const response = await request.get(`${baseURL}/users/1`);
        const body = JSON.parse(await response.body());

        expect(response.status()).toBe(200);
        expect(body.data.id).toBe(1);
        expect(body.data.email).toBeTruthy();
    })

    test('POST request - Create new user', async ({ request }) => {
        const postBody = {
            id: 1000,
        }

        const response = await request.post(`${baseURL}/users`, { postBody });

        expect(response.status()).toBe(201);

    })

    test('POST request - Login', async ({ request }) => {
        const response = await request.post(`${baseURL}/login`, { data: { email: "eve.holt@reqres.in", password: "cityslicka" } });
        const body = JSON.parse(await response.body());

        expect(response.status()).toBe(200);
        expect(body.token).toBeTruthy();
    })

    test('POST request - Login Fail', async ({ request }) => {
        const response = await request.post(`${baseURL}/login`, { data: { email: "eve.holt@reqres.in" } });
        const body = JSON.parse(await response.body());

        expect(response.status()).toBe(400);
        expect(body.error).toBe('Missing password');
    })

    test('PUT request - Update', async ({ request }) => {
        const response = await request.put(`${baseURL}/users/2`, {
            data: {
                name: "fuck",
                job: "Garbage collector"
            }
        });
        const body = JSON.parse(await response.text());

        expect(response.status()).toBe(200);

        expect(body.name).toBe("fuck");
        expect(body.job).toBe("Garbage collector");
    })

    test('Delete request - DELETE', async ({ request }) => {
        const response = await request.delete(`${baseURL}/users/2`);
        expect(response.status()).toBe(204);
    })
})